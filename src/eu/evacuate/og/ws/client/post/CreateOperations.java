package eu.evacuate.og.ws.client.post;

import java.io.IOException;

import org.codehaus.jackson.map.ObjectMapper;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import eu.evacuate.og.ws.client.urlmanager.UrlManager;
import eu.evacuate.og.ws.dto.ControlDTO;
import eu.evacuate.og.ws.dto.DigitalSignDTO;
import eu.evacuate.og.ws.dto.DynamicExitSignDTO;
import eu.evacuate.og.ws.dto.ResponseDTO;
import eu.evacuate.og.ws.dto.STXPhoneDTO;
import eu.evacuate.og.ws.dto.TetraDTO;
import javax.ws.rs.core.MultivaluedMap;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
//import net.sf.json.JSONArray;
import org.primefaces.json.JSONObject;

public class CreateOperations {

//    147.102.5.180
    
    ObjectMapper mapper = new ObjectMapper();
    UrlManager URL = new UrlManager();
    String URLSMS = "http://192.168.0.232:6682/api/network/captive/sms";
    String GetAssociatedPeople = "http://192.168.0.232:6682/api/network/accesspoint/9ac13b44a3b23f6394d5064558f1cf67/associated";

    String getacc = "http://192.168.0.232:6682/api/network/accesspoint";

//    get people associated with : http://192.168.8.231:6682/api/network/accesspoint/9ac13b44a3b23f6394d5064558f1cf67/associated
//http://192.168.1.76:6682/api/network/accesspoint/dd3ea593f5f4381495c60503787d41f9/associated
//http://192.168.1.76:6682/api/network/accesspoint/55603da97ec23c0987c5906d52102023/associated
//http://192.168.1.76:6682/api/network/accesspoint/7e1ef4103a5e3c099745e2660125497b/associated
// get people heard with : http://192.168.1.76:6682/api/network/accesspoint/9ac13b44a3b23f6394d5064558f1cf67/heard
//http://192.168.1.76:6682/api/network/accesspoint/dd3ea593f5f4381495c60503787d41f9/heard
//http://192.168.1.76:6682/api/network/accesspoint/55603da97ec23c0987c5906d52102023/heard
//http://192.168.1.76:6682/api/network/accesspoint/7e1ef4103a5e3c099745e2660125497b/heard
    public static void main(String[] args) throws IOException, JSONException {
        CreateOperations pp = new CreateOperations();
        String ss = pp.GetAccessPoints(null);

        JSONObject jo2 = new JSONObject(ss);
        JSONArray offline = jo2.getJSONArray("offline");
        JSONArray online = jo2.getJSONArray("online");

        for (int i = 0; i < online.length(); i++) {
            ss = pp.GetHeard(online.get(i).toString());
            JSONObject jo21 = new JSONObject(ss);
            System.out.println(jo21.get("count"));
            
            ss = pp.GetAssociated(online.get(i).toString());

        JSONObject jo213 = new JSONObject(ss);
            System.out.println(jo213.get("count"));
        }

//        ss = pp.GetHeard("dd3ea593f5f4381495c60503787d41f9");
//        System.out.println(ss);
//        
//        ss = pp.GetAssociated("dd3ea593f5f4381495c60503787d41f9");
//        System.out.println(ss);
    }

    public void SendMessage(String id,String szMessage)
   {
       CreateOperations pConnector= new CreateOperations();
        ControlDTO pControl=new ControlDTO();
        pControl.setSystems("OG,*");

        try {
            pConnector.evacControlSystem(pControl);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ResponseDTO pReturn=new ResponseDTO();
        TetraDTO pTetra= new TetraDTO();
        pTetra.setProcedure(id);
        pTetra.setMessage(szMessage);
        try {
            pReturn=pConnector.evacTetra(pTetra);
            System.out.println(pReturn.getTaskId());
        } catch (IOException e) {
            e.printStackTrace();
        }
   }
    
    public String GetFRSEvamapp(
            String purl) throws IOException {

        Client client = Client.create();
        WebResource webResource = client
                .resource(purl);

        String szReturn=null;

        szReturn = webResource.get(String.class);
        return szReturn;
    }
    
    public String GetFRSEvamappResponse(
            String purl) throws IOException {

        Client client = Client.create();
        WebResource webResource = client
                .resource(purl);

        String szReturn=null;

        szReturn = webResource.get(String.class);
        return szReturn;
    }
    
    public String PostMessageToFrs(String SMS,String URL) {

        String smsJson = "{\n" +
"  \"originator\":\"EOC\",\n" +
"  \"message\":\""+SMS+"\"\n" +
"}";
        Client client = Client.create();

        WebResource webResource = client.resource(URL);

        try {
            String szReturn =webResource.type("application/json")
                    .post(String.class, smsJson);
//        System.out.println("OK"+szReturn);
        
        JSONObject jo2 = new JSONObject(szReturn);
//        JSONArray frs = jo2.getJSONArray("data");
String idid=jo2.getJSONObject("data").getString("id");
            System.out.println("id:"+idid);
            return idid;
        } catch (Exception ex) {
            System.out.println("Error : " + ex.getMessage());
        }
//        String szReturn = webResource.get(String.class);
       
        return "ÖK";

    }
    
    public String GetAccessPoints(
            ControlDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(getacc);

        String szReturn=null;

        szReturn = webResource.get(String.class);
        return szReturn;
    }

    public String GetHeard(
            String mac) throws IOException {

        Client client = Client.create();
        WebResource webResource = client
                .resource(getacc + "/" + mac + "/heard");

        String szReturn;

        szReturn = webResource.get(String.class);
        return szReturn;
    }

    public String GetAssociated(
            String mac) throws IOException {

        Client client = Client.create();
        WebResource webResource = client
                .resource(getacc + "/" + mac + "/associated");

        String szReturn;

        szReturn = webResource.get(String.class);
        return szReturn;
    }

    public String SendSMMToAll(String SMS) {

        String smsJson = "{    \"filter\": \"none\",    \"content\": \"" + SMS + "\"}";
        Client client = Client.create();

        WebResource webResource = client.resource(URLSMS);

        try {
            webResource.type("application/json")
                    .put(String.class, smsJson);
        } catch (Exception ex) {
            System.out.println("Error : " + ex.getMessage());
        }
        return "ÖK";

    }

    public String SendSMMToInRange(String SMS) {

        String smsJson = "{    \"filter\": \"associated\",    \"content\": \"" + SMS + "\"}";
        Client client = Client.create();
        WebResource webResource = client.resource(GetAssociatedPeople);

        try {
            webResource.type("application/json")
                    .put(String.class, smsJson);
        } catch (Exception ex) {
            System.out.println("Error : " + ex.getMessage());
        }
        return "OK";

    }

    public boolean evacControlSystem(
            ControlDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlControl());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return true;
    }

    public ResponseDTO evacTetra(
            TetraDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlTetra());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return mapper.readValue(szReturn, ResponseDTO.class);
    }

    public ResponseDTO evacDigitalSign(
            DigitalSignDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);
        System.out.println("Json to OG : " + ControlJSON);
        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlDigitalSign());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return mapper.readValue(szReturn, ResponseDTO.class);
    }

    public ResponseDTO evacDigitalExitSign(
            DynamicExitSignDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlDynamicExitSign());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return mapper.readValue(szReturn, ResponseDTO.class);
    }

    public ResponseDTO evacSTXPhone(
            STXPhoneDTO pControlDTO) throws IOException {

        String ControlJSON = mapper
                .writeValueAsString(pControlDTO);

        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getUrlSTXPhone());

        String szReturn;

        szReturn = webResource.type("application/json")
                .post(String.class, ControlJSON);
        return mapper.readValue(szReturn, ResponseDTO.class);
    }

}
