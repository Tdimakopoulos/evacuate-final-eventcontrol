/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.play.sound;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.audio.*;

/**
 * A simple Java sound file example (i.e., Java code to play a sound file).
 * AudioStream and AudioPlayer code comes from a javaworld.com example.
 *
 * @author alvin alexander, devdaily.com.
 */
public class playfile {

    public static void main(String[] args)
            throws Exception {
        // open the sound file as a Java input stream
        String gongFile = "c:\\wav\\auto.wav";
        InputStream in = new FileInputStream(gongFile);

        // create an audiostream from the inputstream
        AudioStream audioStream = new AudioStream(in);

        // play the audio clip with the audioplayer class
        AudioPlayer.player.start(audioStream);
    }
    
    public void PlaySound(String filename)
    {
//        String gongFile = "c:\\wav\\1.wav";
        InputStream in;
        try {
            in = new FileInputStream(filename);
            
        // create an audiostream from the inputstream
        AudioStream audioStream = new AudioStream(in);

        // play the audio clip with the audioplayer class
        AudioPlayer.player.start(audioStream);
        
        } catch (FileNotFoundException ex) {
            Logger.getLogger(playfile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(playfile.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
