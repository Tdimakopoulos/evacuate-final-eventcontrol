/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evacuatestartstop;

import com.indra.sofia2.ssap.kp.implementations.rest.SSAPResourceAPI;
import com.indra.sofia2.ssap.kp.implementations.rest.exception.ResponseMapperException;
import com.indra.sofia2.ssap.kp.implementations.rest.resource.SSAPResource;
import eu.evacuate.controllertm.tm;
import eu.evacuate.og.ws.client.post.CreateOperations;
import eu.play.sound.playfile;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.exus.alarm.publishalarm;
import org.exus.timestamp.timestamp;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author inno
 */
public class EvacuateStartStop {

    static String szURL = "http://192.168.0.208:8080/sib/services/api_ssap/";
    static SSAPResourceAPI p = null;
    static String sessionkey;
    static SSAPResource pres = new SSAPResource();

    public void writeFile2(String file, String line) throws IOException {
        FileWriter fw = new FileWriter("c:\\evacctrl\\" + file);

        fw.write(line);

        fw.close();
    }

    public void DeleteFile(String filepath) {
        try {

            File file = new File("c:\\evacctrl\\" + filepath);

            if (file.delete()) {
                System.out.println(file.getName() + " is deleted!");
            } else {
//                System.out.println("Delete operation is failed.");
            }

        } catch (Exception e) {

//            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws ResponseMapperException, IOException, JSONException {

        System.out.println("Starting Evacuate Start/End Server (Version : Final) !");
        publishalarm pa = new publishalarm();
        EvacuateStartStop pss = new EvacuateStartStop();

        pa.PublishAlarm();
        String istart = pa.QueryOnto();

        String istart2 = pa.QueryOnto2();
        while (true) {
            try {
                Thread.sleep(10000);
                System.out.println("Check");
                String inew = pa.QueryOnto();
                String inew2 = pa.QueryOnto2();

                if (istart.equalsIgnoreCase(inew)) {
                    System.out.println("Nothing");
                } else {
                    System.out.println("Evacuate Start!!");
                    pss.writeFile2("vv.vv", "1");
                    CreateOperations pop = new CreateOperations();
                    pop.SendSMMToAll("Evacuation Start");
//                    pop.SendMessage("89136", "Evacuation Start!");
//                    pop.SendMessage("89137", "Evacuation Start!");

                    istart = inew;
                    tm pp = new tm();
                    pp.Open();
                    playfile pfile = new playfile();
                    pfile.PlaySound("c:\\wav\\manual.wav");
                }

                if (istart2.equalsIgnoreCase(inew2)) {
                    System.out.println("Nothing");
                } else {
                    System.out.println("Evacuate Ends!!");
                    pss.DeleteFile("vv.vv");
                    CreateOperations pop = new CreateOperations();
                    pop.SendSMMToAll("Evacuation Ends");
                    pop.SendMessage("89136", "Evacuation Ends!");
                    pop.SendMessage("89137", "Evacuation Ends!");

                    
                    
                    istart2 = inew2;
                    tm pp = new tm();
                    pp.Close();
                    pp.Recover();
                }

            } catch (InterruptedException ex) {
                System.out.println("error on wait");
            }

        }

    }

}
