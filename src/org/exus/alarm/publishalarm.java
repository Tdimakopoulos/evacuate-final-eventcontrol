package org.exus.alarm;

import com.indra.sofia2.ssap.kp.implementations.rest.SSAPResourceAPI;
import com.indra.sofia2.ssap.kp.implementations.rest.exception.ResponseMapperException;
import com.indra.sofia2.ssap.kp.implementations.rest.resource.SSAPResource;
import eu.evacuate.og.ws.client.post.CreateOperations;
import eu.evacuate.og.ws.dto.ControlDTO;
import eu.evacuate.og.ws.dto.ResponseDTO;
import eu.evacuate.og.ws.dto.TetraDTO;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.exus.timestamp.timestamp;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Thomas Dimakopoulos eVACUATE_WP8SensorsStatus
 * http://vineyard-ws.telesto.gr/SonemaEvacuate/
 *
 * και για το web service
 *
 * http://vineyard-ws.telesto.gr/SonemaEvacuate/jersey/jsonservice/demotext
 *
 */
public class publishalarm {

    static String szURL = "http://192.168.0.208:8080/sib/services/api_ssap/";
    static SSAPResourceAPI p = null;
    static String sessionkey;
    static SSAPResource pres = new SSAPResource();

     public void SendMessage(String id,String szMessage)
   {
       CreateOperations pConnector= new CreateOperations();
        ControlDTO pControl=new ControlDTO();
        pControl.setSystems("OG,*");

        try {
            pConnector.evacControlSystem(pControl);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ResponseDTO pReturn=new ResponseDTO();
        TetraDTO pTetra= new TetraDTO();
        pTetra.setProcedure(id);
        pTetra.setMessage(szMessage);
        try {
            pReturn=pConnector.evacTetra(pTetra);
            System.out.println(pReturn.getTaskId());
        } catch (IOException e) {
            e.printStackTrace();
        }
   }

    public static void main(String[] args) throws ResponseMapperException, IOException, JSONException {

    
            publishalarm pp= new publishalarm();
            pp.SendMessage("8913", "Test evacuate");
            
//        System.out.println("Starting Evacuate Start/End Server !");
//        publishalarm pa = new publishalarm();
//        
//        pa.PublishAlarm();
//        int istart = pa.QueryOnto();
//
//        int istart2 = pa.QueryOnto2();
//        while (true) {
//            try {
//                Thread.sleep(30000);
//                System.out.println("Check");
//                int inew = pa.QueryOnto();
//                int inew2 = pa.QueryOnto2();
//
//                if (istart == inew) {
//                    System.out.println("Nothing");
//                } else {
//                    System.out.println("Evacuate Start!!");
//                    CreateOperations pop = new CreateOperations();
//                    pop.SendSMMToAll("Evacuation Start");
//                }
//
//                if (istart2 == inew2) {
//                    System.out.println("Nothing");
//                } else {
//                    System.out.println("Evacuate Ends!!");
//                    CreateOperations pop = new CreateOperations();
//                    pop.SendSMMToAll("Evacuation Ends");
//                }
//
//            } catch (InterruptedException ex) {
//                System.out.println("error on wait");
//            }
//
//        }

    }

    public void close() {
        pres.setLeave(true);
    }

    
    //SocialKPOnly
    //77bcfc0c6ddb4c92bc9f3d463cea27e6
    public void PublishAlarm() throws ResponseMapperException {
        if (p == null) {
            p = new SSAPResourceAPI(szURL);
        }

        pres.setJoin(true);

        pres.setToken("77bcfc0c6ddb4c92bc9f3d463cea27e6");
        pres.setInstanceKP("SocialKPOnly:SocialKPOnlyIns");

        Response presponse = p.insert(pres);
        if (presponse.getStatus() == 200) {
            //good
            sessionkey = p.responseAsSsap(presponse).getSessionKey();
            System.out.print("Session Key : ");
            System.out.println(sessionkey);
        } else {
            System.out.println("error : " + presponse.getStatus());
        }

    }

    public void NowInsert() {

    }

    public void NowInsertFire() {

    }

    public void NowInsertCrowding() {
    }

    public void NowInsertExplosion() {
    }

    public void NowInsertFallenPerson() {
    }

    public void NowInsertTransportation() {
    }

    public void ResetCount() {
        
        timestamp pps = new timestamp();
        String sziii = "\"WP8ResetCounters\":{ \"feature\":\"EOC Reset Counters\",\"timestamp\":\""+pps.GetTimeStamp()+"\"}";
        SSAPResource presl = new SSAPResource();
        presl.setSessionKey(sessionkey);
        presl.setOntology("eVACUATE_WP8ResetCounters");
        presl.setData(sziii);

        Response presponse = p.insert(presl);
        System.out.println("Reset Count Status Delete : " + presponse.getStatus());
    }


    public String QueryOnto() throws IOException, ResponseMapperException, JSONException {
        Response presponse = p.query(sessionkey, "eVACUATE_WP5StartEvacuation ", "select contextData.timestamp from eVACUATE_WP5StartEvacuation ORDER by contextData.timestamp desc limit 1", "", "SQLLIKE");

        System.out.println("Query Status : " + presponse.getStatus());
        String dd = p.responseAsSsap(presponse).getData().toString();
        
        System.out.println("org.exus.alarm.publishalarm.QueryOnto() : " +dd);
//        
//        JSONArray inputArray = new JSONArray(dd);
//        System.out.println("Total Number : "+inputArray.get(0));
//        JSONObject d=inputArray.get(0);
//        System.out.println("Total Number : "+d.getString("timestamp")));
//        return inputArray.length();
return dd;

    }

    public String QueryOnto2() throws IOException, ResponseMapperException, JSONException {
        Response presponse = p.query(sessionkey, "eVACUATE_WP5EndEvacuation ", "select contextData.timestamp from eVACUATE_WP5EndEvacuation ORDER by contextData.timestamp desc limit 1", "", "SQLLIKE");

        System.out.println("Query Status : " + presponse.getStatus());
        String dd = p.responseAsSsap(presponse).getData().toString();
//        JSONArray inputArray = new JSONArray(dd);
//        System.out.println(inputArray.length());
//        return inputArray.length();
return dd;

    }

    public String GetStatus() throws IOException, ResponseMapperException, JSONException {
        String szret = "";
        Response presponse = p.query(sessionkey, "eVACUATE_WP8ActuatorRoutes", "select * from eVACUATE_WP8ActuatorRoutes", "", "SQLLIKE");

        String dd = p.responseAsSsap(presponse).getData();//.getData().toString();
        System.out.println("Query Status : " + dd);
        JSONArray inputArray = new JSONArray(dd);
        for (int i = 0; i < inputArray.length(); i++) {
            JSONObject jo = inputArray.getJSONObject(i);
            JSONObject jo2 = jo.getJSONObject("WP8ActuatorRoutes");

            szret = szret + jo2.getString("device_type") + " ID : " + jo2.getString("device_id") + "Previous Command " + jo2.getString("previous_command") + "Command " + jo2.getString("command") + "<br/>";

        }

        return szret;
    }

}
